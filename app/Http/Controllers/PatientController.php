<?php
namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Paginator;
use App\Patient;
use App\User;
use App\Picture;
use App\Donation;
use App\Sponsor;
use App\Redeem;
use App\Stories;
use Carbon\Carbon;
use App\Notifications\NotifyUsers;
use Illuminate\Support\Facades\DB;
class PatientController extends Controller
{
    public function redeem(Request $request){
        $redeem = Patient::findorfail($request->id);
        if($redeem['goal'] != $redeem['TotalRedeem']) {
            $redeem->status = "partial";
            $redeem->expirydateV = $request->expirydate;
            $redeem->newgoal = $redeem['goal'] - $redeem['TotalRedeem'];
            $redeem->redeemed = $redeem['goal'] - $redeem['newgoal'];
            $redeem->save();
        }
        else {
            $redeem->status = "full";
            $redeem->expirydateV = $request->expirydate;
            $redeem->newgoal = $redeem['goal'] - $redeem['TotalRedeem'];
            $redeem->redeemed = $redeem['goal'] - $redeem['newgoal'];
            $redeem->save();
        }

        //current story
        $user = Auth::id();
        
        $patientDetails = Patient::where('userid', $user)->where('status', 'approved')->get();

//sponsors                      
$sponsor = Sponsor::where('userid', $user)->get();
$donation = Donation::get();
$sponsorCollect = new Collection();
foreach($sponsor as $spr){
    foreach($donation as $dnr){
        if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
            $sponsorCollect->push($spr);
            }
        }
}

//redeem
       
        $redeemdetails = Patient::where('userid', $user)->where('status', 'partial')->orWhere('status', 'full')->get();

        return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'patientDetails'=>$patientDetails, 'redeemdetails'=>$redeemdetails]);
 
    }


    public function voucher($patientid){
        $details = Patient::findorfail($patientid);
        // $details = Patient::where('userid', $user)->where('status', 'approved')->get();
        $expirydate = date('Y-m-d', strtotime('+1 years'));
        return view('confirm')->with(['expirydate'=>$expirydate, 'details'=>$details]);
    }




    public function displayPatient(Request $request){
        if(Auth::user()->role == "admin") {
            $pnt = Patient::get();

         $data = Patient::where('status', 'approved')->orWhere('status', 'partial')->whereRaw('goal != TotalRedeem')->get();
         $success = Patient::whereRaw('goal = TotalRedeem')->get();
            return view('displayUsers')->with(['data'=>$data, 'success'=>$success]);
        }


            $data = Patient::where('status', 'approved')->orWhere('status', 'partial')->whereRaw('goal != TotalRedeem')->get();
            $success = Patient::whereRaw('goal = TotalRedeem')->get();
         
            return view('homepage')->with(['data'=>$data, 'success'=>$success]);
            

        
    }

   
    public function patient($patientid){
	$patient = Patient::findorfail($patientid);       
    $story = Stories::where('patientid', $patient['patientid'])->first();
    $pic = Picture::where('storyid', $story['storyid'])->paginate(1);
	return view('singlelist')->with(['patient'=>$patient, 'story'=>$story,'pic'=>$pic]);
	}


    public function updatepage($patientid){
    //$patient = Patient::findorfail($patientid);
    $ups = stories::where('patientid', $patientid)->where('role', null)->paginate(1);
    return view('updatepage')->with(['ups'=>$ups]);
    }



    public function newPatient(){
        
    	return view('patientsdetail');
    }
    public function savePatient(Request $request){
        //$picture = Picture::all();
        $id = Auth::id();
        if($request->hasFile('profile')){
            $files = $request->file('profile');
            $origextension = $files->getClientOriginalExtension();
           $origname = $files->getClientOriginalName();
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;
            $files->storeAs('public/picture', $storefile);
            
            
        $patient = new Patient();
        $patient->userid = $id;
        $patient->goal = $request->goal;
        $patient->filename = $storefile;
        $patient->status = "pending";
        $patient->patientname = $request->bname;
        $patient->illness = $request->illness;
        $patient->save();

        }


        $story = new Stories();
        $story->patientid = $patient->patientid;
        $story->storytitle = $request->title;
        $story->story = $request->story;
        $story->role = "main";
        $story->save();
    	$insertid = $patient->patientid;
        

        if($request->hasFile('medicalAbstract')){
            $files = $request->file('medicalAbstract');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "medicalAbstract";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }


        if($request->hasFile('medicalCertificate')){
            $files = $request->file('medicalCertificate');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "medicalCertificate";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        } 

        if($request->hasFile('validID')){
            $files = $request->file('validID');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "validID";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }

        if($request->hasFile('hospitalBill')){
            $files = $request->file('hospitalBill');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "hospitalBill";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }

        if($request->hasFile('breakdownExpenses')){
            $files = $request->file('breakdownExpenses');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "breakdownExpenses";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }

        

    	return redirect(url('/list/'.$insertid.'/view/'));
    }




    public function updateStory($patientid){
        return view('update')->with(['patient'=>$patientid]);
    }

    public function saveupdateStory(Request $request){
        $story = new Stories();
        $story->patientid = $request->patientid;
        $story->storytitle = $request->updatetitle;
        $story->story = $request->story;
        $story->role = null;
        $story->save();
        $insertid = $request->patientid;
        

        if($request->hasFile('file')){
            $files = $request->file('file');
            foreach($files as $file){

           $origextension = $file->getClientOriginalExtension();
           $origname = $file->getClientOriginalName();
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $file->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;

            $picture->save();
            }
        }

        return redirect(url('/update/'.$insertid.'/view/'));
    }



    public function donors($patientid){
        $patient = Patient::findorfail($patientid);

        //total of all donations
        foreach($patient as $pnt){
             $donors = Donation::where('patientid', $patientid)->get(); 
        }
        $total = 0;
        foreach($donors as $amount){
            $count = $amount->sponsor->voucherValue;
            $total += $count;
        }

        foreach($patient as $pnt){
             $nonanonymous = Donation::where('patientid', $patientid)->where('sponsorName','!=', 'anonymous')->get(); 
        }

        //for all total sa anonymous donations
        foreach($patient as $pnt){
             $anonymous = Donation::where('patientid', $patientid)->where('sponsorName', 'anonymous')->get(); 
        }
        $totalanonymous = 0;
        foreach($anonymous as $amount){
            $count = $amount->sponsor->voucherValue;
            $totalanonymous += $count;
        }
       
        return view('donors')->with(['total'=>$total, 'totalanonymous'=>$totalanonymous, 'nonanonymous'=>$nonanonymous]);
    }


}
