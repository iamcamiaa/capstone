<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Billing;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class SponsorController extends Controller
{
  public function buyvoucher(){
        return view('buyvoucher');
    }

    // angel edited
    public function savevoucher(Request $request){
    if($request->qty100 < 0 || $request->qty500 < 0 || $request->qty1000 < 0 || $request->qty5000 < 0){
      return Redirect::back()->with('error', true);
    }else{
        $billing = new Billing();
        $name = Auth::user()->fname." ".Auth::user()->lname;
        $billing->userid = Auth::id();
        $billing->receipt = $request->receipt;
        $billing->accountHolder = $name;
        $billing->ifReceived= "pending";
        $billing->save();
      for($i = 0; $i < $request->qty100; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 100;
        $user->status = "pending";
        $user->save();
      }
      for($i = 0; $i < $request->qty500; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 500;
        $user->status = "pending";
        $user->save();
      }  
        for($i = 0; $i < $request->qty1000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 1000;
        $user->status = "pending";
        $user->save();;
        }
        for($i = 0; $i < $request->qty5000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 5000;
        $user->status = "pending";
        $user->save();
        }
        
      }
          return Redirect::back()->with('success', true);
    }
      
  public function newSponsor($patientid){
        $patient = Patient::findorfail($patientid);
        $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->get();   
        return view('sponsorDonate')->with(['patient'=>$patient, 'data'=>$data]);
    }



  public function getDen()
    {
       $overall = 0;
$countvalue = Sponsor::select('voucherValue')->where('userid', Auth::id())->where('status', null)->distinct()->get();
      $value = array();
      $d = new Collection();
      foreach ($countvalue as $key) {
        $cnt = Sponsor::where('voucherValue',$key->voucherValue)->where('userid', Auth::id())->where('status', null)->get()->count();
        $total = $key->voucherValue * $cnt;
        $overall = $total+=$overall;

        if($key->voucherValue == 100){
          $value['value'] = '/images/v_100.jpg';
        }else if($key->voucherValue == 500){
          $value['value'] = '/images/v_500.jpg';
        }else if($key->voucherValue == 1000){
          $value['value'] = '/images/v_1000.jpg';
        }else if($key->voucherValue == 5000){
          $value['value'] = '/images/v_5000.jpg';
        }

        $value['count'] = $cnt;
        $value['total'] = (string)$overall;
        $d->push($value);    
      }

      return $d;
  }


    public function saveSponsor(Request $request){ 
      $patient = Patient::findorfail($request->patientid);
      $lacking = $patient['goal'] - $patient['donations'];
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();
      $name = Auth::user()->fname." ".Auth::user()->lname;
      $radio = Input::get('anonymous') == true ? $request->anonymous:$name;
      $amount = $request->amount;
      if($amount == 0){
        return Redirect::back()->with('message', true);
      }

      $total = $sponsor->sum('voucherValue'); 
      $collect = new Collection();
      $V = new Collection();
      $amt = $amount;
if($lacking < $amount){
  return Redirect::back()->with('donate', true)->with('lacking', $lacking); 
}
if($amt <= $total && $amount != 0){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            $amt -= $c['voucherValue'];
          }
        }
      

        $avblVoucher =  $V->sum('voucherValue');
      if($amt == 0){
        $total = $patient->TotalRedeem + $V->sum('voucherValue');
        $patient->TotalRedeem = $total;
        $patient->save();
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->sponsorName = $radio;
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }else if ($avblVoucher != 0){      
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }else
          return Redirect::back()->with('alert', true);
}return Redirect::back()->with('notenough', true);
    }



    public function saveSponsorAny(Request $request){
      $amount = $request->amount;
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();  
      $total = $sponsor->sum('voucherValue'); 
      $name = Auth::user()->fname." ".Auth::user()->lname;
      $radio = Input::get('anonymous') == true ? $request->anonymous:$name;
      if($amount == 0){
        return Redirect::back()->with('message', true);
      }

      $collect = new Collection();
      $V = new Collection();
      $amt = $amount;
if($amt <= $total){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            $amt -= $c['voucherValue'];
          }
        }
        

        $avblVoucher =  $V->sum('voucherValue');
        if($amt == 0){
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = null;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->sponsorName = $radio;
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }else if ($avblVoucher != 0){    
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }else
          return Redirect::back()->with('alert', true);
}
return Redirect::back()->with('notenough', true);
    }

   
    public function newSponsorAny(){
      

     $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->get();

         return view('donateAny')->with(['data'=>$data]);
    }

     public function getValue()
    {
      return view('viewvouchers');
    }


}
