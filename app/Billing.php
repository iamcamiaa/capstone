<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    //
     protected $primaryKey = 'billing_id';
      public function user(){
    	return $this->hasOne('App\User', 'id', 'userid');
    } 
     public function userName(){
    	return $this->hasOne('App\User','id','userid');
    }
	public function sponsor(){
    	return $this->hasMany('App\Sponsor','sponsor_serial','sponsor_serial');
    }
}
